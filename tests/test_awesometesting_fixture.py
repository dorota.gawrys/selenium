import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


@pytest.fixture
def browser():
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)
    # część która wykona się przed każdym testem
    browser.get('http://www.awesome-testing.blogspot.com')
    # coś do przekazujemy do testu
    yield browser
    # część która wykona się po każdym teście
    browser.quit()


def test_post_count(browser):
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager

    posts = browser.find_elements(By.CSS_SELECTOR,'.post-title')
    # Asercja że lista ma 4 elementy
    assert len(posts) == 4


def test_post_count_after_search(browser):
    # Szukanie
    browser.find_element(By.CSS_SELECTOR, '.gsc-input input').send_keys('Selenium')
    browser.find_element(By.CSS_SELECTOR, '.gsc-search-button input').click()

    # Czekanie na stronę

    # Pobranie listy tytułów
    posts = browser.find_elements(By.CSS_SELECTOR, '.post-title')

    # Asercja że lista ma 20 elementów
    assert len(posts) == 20


def test_post_count_on_cypress_label(browser):
    # Inicjalizacja elementu z labelką
    label = browser.find_element(By.LINK_TEXT, '2019')

    # Kliknięcie na labelkę
    label.click()
    # Czekanie na stronę
    time.sleep(4)
    # Pobranie listy tytułów
    posts = browser.find_elements(By.CSS_SELECTOR, '.post-title')
    # Asercja że lista ma 1 element
    assert len(posts) == 10
