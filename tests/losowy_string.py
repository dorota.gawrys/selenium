import string
import random

# to trzeba dopisać do projektu
project_name = get_random_string(10)
prefix = get_random_string(6)

project.page_verify(project_name)


def get_random_string(length):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))
