import time

from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

administrator_email = 'administrator@testarena.pl'

@pytest_fixture
def browser():
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)
    # część która wykona się przed każdym testem
    browser.get('https://testarena.pl')
    # coś do przekazujemy do testu
    yield browser
    # część która wykona się po każdym teście
    browser.quit()

def test_my_first_chrome_selenium_test(browser):
    #znalezienie elementu gdzie wpisujemy email
    emailInput = browser.find_element(By.CSS_SELECTOR, value='#email')
    emailInput.send_keys('administrator@testarena.pl')

    #znalezienie gdzie wpisujemy hasło
    PasswordInput = browser.find_element(By.CSS_SELECTOR, value='#password')
    PasswordInput.send_keys('sumXQQ72$L')
    submitButton = browser.find_element(By.CSS_SELECTOR, value='#login')
    submitButton.click()

    #Asercja`
    user_email = browser.find_element(By.CSS_SELECTOR, '.user-info small').text
    assert user_email == 'administrator@testarena.pl'


    # Zamknięcie przeglądarki
    time.sleep(4)

