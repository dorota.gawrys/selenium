import time

from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


def test_searching_in_duckduckgo():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)

    # Otwarcie strony duckduckgo
    browser.get('https://www.duckduckgo.com')

    # Znalezienie paska wyszukiwania

    Wordsearching = browser.find_element(By.CSS_SELECTOR, "#searchbox_input")
    Wordsearching.send_keys('4 testers')

    # $$("#searchbox_input")- selektor dla wyszukiwania
    # $$("[aria-label=Search]") - selektor dla lupki w wyszukiwarce

    # Znalezienie guzika wyszukiwania (lupki)
    searchingbuttom = browser.find_element(By.CSS_SELECTOR, "[aria-label=Search]")
    searchingbuttom.click()

    # selektor na wyniki wyszukiwania [data - testid = result - title - a] span
    # Asercje że pierwszy element ma zawierać tytuł
    time.sleep(3)
    titles = browser.find_elements(By.CSS_SELECTOR, '[data-testid=result-title-a] span')
    assert '4_testers - Kurs Tester Oprogramowania' in titles[0].text

    # Szukanie Vistula University

    # Sprawdzenie że pierwszy wynik ma tytuł 'Vistula University in Warsaw'

    # Zamknięcie przeglądarki
    time.sleep(3)
    browser.quit()
