import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

from pages.arena.Project_page import ProjectPage
from pages.arena.homepage import HomePage
from pages.arena.login_page import LoginPage

administrator_email = 'administrator@testarena.pl'


@pytest.fixture
def browser():
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)
    login_page = LoginPage(browser)
    login_page.visit()
    login_page.login(administrator_email, 'sumXQQ72$L')

    # coś do przekazujemy do testu
    yield browser
    # część która wykona się po każdym teście
    browser.quit()


def test_testarena_login(browser):
    user_email = browser.find_element(By.CSS_SELECTOR, '.user-info small').text
    assert user_email == administrator_email


def test_testarena_logout(browser):
    home_page = HomePage(browser)
    home_page.click_logout()

    assert '/zaloguj' in browser.current_url
    assert browser.find_element(By.CSS_SELECTOR, '#password').is_displayed()


def test_add_messages(browser):
    browser.find_element(By.CSS_SELECTOR, '.top_messages').click()
    # expected conditions czyli czkeamy aż coś będzie możliwe, aż element będzie miał okreslony stan
    # zawsze trzeba zaimportować wait najpietw
    # konstruktor przyjmuje dwa parametry, nasz obiekt przeglądarki drivera i maksymalny czas czekania
    wait = WebDriverWait(browser, 10, poll_frequency=0.2)
    # pythonowa tupla /krotka
    message_area = (By.CSS_SELECTOR, '#j_msgContent')
    wait.until(expected_conditions.element_to_be_clickable(message_area))

    browser.find_element(By.CSS_SELECTOR, '#j_msgContent').send_keys('Wiadomość')


def test_searching_project(browser):
    home_page = HomePage(browser)
    home_page.click_on_administration_link()

    project_page = ProjectPage(browser)
    project_page.search_for_project('kamil')
    project_page.verify_projects_found('kamil')

