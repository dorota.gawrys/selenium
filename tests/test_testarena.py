import time

from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


def test_my_first_chrome_selenium_test():
    # Setup drivera. Driver jest pobierany automatycznie przez bibliotekę webdriver-manager
    administrator_email = 'administrator@testarena.pl'
    service = Service(ChromeDriverManager().install())
    # Uruchomienie przeglądarki
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)

    # Otwarcie strony testareny - pierwsze użycie Selenium API
    browser.get('http://demo.testarena.pl/zaloguj')

    #znalezienie elementu gdzie wpisujemy email
    emailInput = browser.find_element(By.CSS_SELECTOR, value='#email')
    emailInput.send_keys('administrator@testarena.pl')

    #znalezienie gdzie wpisujemy hasło
    PasswordInput = browser.find_element(By.CSS_SELECTOR, value='#password')
    PasswordInput.send_keys('sumXQQ72$L')
    submitButton = browser.find_element(By.CSS_SELECTOR, value='#login')
    submitButton.click()

    #Asercja
    user_email = browser.find_element(By.CSS_SELECTOR, '.user-info small').text
    assert user_email == 'administrator@testarena.pl'


    # Zamknięcie przeglądarki
    time.sleep(4)
    browser.quit()




