from selenium.webdriver.common.by import By

from pages import home


class HomePage:

    def __init__(self, browser):
        self.browser = browser


    def click_logout(self):
        self.browser.find_element(By.CSS_SELECTOR, '.icons-switch').click()

    def click_on_administration_link(self):
        self.browser.find_element(By.CSS_SELECTOR, '.header_admin a').click()
